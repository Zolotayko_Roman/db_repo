USE [Employee]
GO
/****** Object:  StoredProcedure [dbo].[XmlUpdater7]    Script Date: 11/8/2018 8:39:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER proc [dbo].[XmlUpdater7]
(
@Request xml
)
as
begin

begin transaction 
	begin try  
		merge TestTable as target
		using (
			select
				Item.value('EmployeeId[1]','int') AS EmployeeId,
				Item.value('FirstName[1]','varchar(50)') AS FirstName
			from
				@Request.nodes('/*/*') AS XmlData(Item)) as source
		on (target.EmployeeId = source.EmployeeId) 
		when matched and
			(target.FirstName <> source.FirstName) 
		then update 
			set
				target.FirstName = SOURCE.FirstName
		when not matched by target then 
			insert (EmployeeId, FirstName) 
			values (source.EmployeeId, source.FirstName)
		when not matched by source then 
			delete;
 end try
 begin catch
        if @@trancount > 0
		exec sp_xml_removedocument
        rollback transaction
end catch
commit;
end