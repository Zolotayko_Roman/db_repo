create proc [dbo].[NewVelTable](@Employee_details [NewEmp] readonly) as
begin
declare @MyTable NewEmp
declare @MyId nvarchar(50)
declare new_curs cursor local for
select id
from @Employee_details
open new_curs
fetch next from new_curs into @MyId
while @@FETCH_STATUS=0
begin 
exec Generator TestTable,@MyId output
select * into #temp from @Employee_details
UPDATE #temp SET id=@MyId
insert into @MyTable 
select* from
#temp
fetch next from new_curs into @MyId
drop table #temp
end
close new_curs
end