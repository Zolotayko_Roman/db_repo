create proc [dbo].[Generator]
    @table NVARCHAR(50),
    @id NVARCHAR(30) OUTPUT
AS
BEGIN
    SET @table = SUBSTRING (@table, 0, 4)
  SET @id = @table + ' ' + CONVERT(NVARCHAR(30), SYSDATETIME())
END