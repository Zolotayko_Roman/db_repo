--insert Employee values ('1','1','1','MyFirst','MySecond','2000-10-11');
--insert into AddressTable (Address)
--values ('Heroes of Dnepr');

--select Phones + id as MyPhone, id
--into AnotherAddressTable
--from Phones

--use Employee
--select FirstName, SecondName
--from Employee
--where FirstName = 'Roman'

--select Post,Salary
--from PostAndSalary
--where Salary>1000

--select Employee.FirstName,Employee.SecondName, PostAndSalary.Post, PostAndSalary.Salary
--from Employee
--join PostAndSalary on Employee.PostSalaryId=PostAndSalary.id
--where PostAndSalary.Salary > 1000
--order by PostAndSalary.Salary


--select * 
--from Customers
--where Region = 'SP'
--union select *
--from Customers
--where City='Sao Paulo'



--select FirstName,  Region, City
--from Employees
--where Region='WA'
--except select FirstName,  Region, City
--from Employees
--where City='Tacoma'


--select *
--from Products
--where UnitPrice > (select AVG(UnitPrice) from Products)
--order by UnitPrice

--select ContactName,Region,Country,COUNT(Region) as Regions
--from Suppliers
--where Country='USA'
--Group by ContactName,Region,Country
--Having count(Region) > 1


select 
	(select CategoryName FROM Categories
	where CategoryID = Products.CategoryID) AS CategoryName, 
	max(UnitPrice) AS MaxPrice
from Products
group by CategoryID

select E.FirstName, E.LastName, T.TerritoryDescription, R.RegionDescription
from Employees as E
join EmployeeTerritories as ET
	on E.EmployeeID = ET.EmployeeID
join Territories as T
	on ET.TerritoryID = T.TerritoryID
join Region as R
	on T.RegionID = R.RegionID

--select LastName, FirstName, EmployeeTerritories.TerritoryID,Territories.TerritoryDescription, Territories.RegionID
--from Employees
--full join EmployeeTerritories on Employees.EmployeeID = EmployeeTerritories.EmployeeID
--full join Territories on Territories.TerritoryID=EmployeeTerritories.TerritoryID
--where LastName is null

